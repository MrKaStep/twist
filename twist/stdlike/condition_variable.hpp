#pragma once

#if defined(FLAKY)

#include <twist/flaky/condvar.hpp>

namespace twist {
using condition_variable = flaky::FlakyConditionVariable;  // NOLINT
}  // namespace twist

#else

#include <condition_variable>

namespace twist {
using std::condition_variable;
}  // namespace twist

#endif
