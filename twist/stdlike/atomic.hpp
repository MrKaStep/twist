#pragma once

#if defined(FLAKY)

#include <twist/flaky/atomic.hpp>

namespace twist {

template <typename T>
using atomic = flaky::FlakyAtomic<T>;  // NOLINT
}  // namespace twist

#else

#include <atomic>

namespace twist {
using std::atomic;
}  // namespace twist

#endif
