#pragma once

#if defined(FLAKY)

#include <twist/flaky/mutex.hpp>

namespace twist {
using mutex = flaky::FlakyMutex;  // NOLINT
}  // namespace twist

#else

#include <mutex>

namespace twist {
using std::mutex;
}  // namespace twist

#endif
