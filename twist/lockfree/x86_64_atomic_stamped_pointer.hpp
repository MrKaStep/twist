#pragma once

#include <twist/stdlike/atomic.hpp>
#include <twist/support/bithacks.hpp>

#include <cstddef>

namespace twist {
namespace lockfree {

static_assert(sizeof(uintptr_t) == 8, "Platform not supported");

// AtomicStampedPointer packs pointer and stamp (revision) into a single 64-bit
// word Implementation relies on the fact that modern processors use 48-bit
// addresses, so upper 16 bits of pointer can be used to store stamp See
// https://en.wikipedia.org/wiki/X86-64#Virtual_address_space_details

template <typename T>
class X86AtomicStampedPointer {
 public:
  // Acts as ordinary pointer:
  // ptr->FooBar()
  // if (ptr) { ... }
  struct StampedPtr {
    T* raw_ptr_;
    size_t stamp_;

    StampedPtr(T* ptr = nullptr, size_t stamp = 0)
        : raw_ptr_(ptr), stamp_(stamp) {
    }

    T* operator->() const {
      return raw_ptr_;
    }

    T& operator*() const {
      return *raw_ptr_;
    }

    explicit operator bool() const {
      return raw_ptr_ != nullptr;
    }
  };

 public:
  explicit X86AtomicStampedPointer(T* ptr = nullptr, size_t stamp = 0)
      : packed_ptr_(Pack({ptr, stamp})) {
  }

  StampedPtr Load() const {
    return Unpack(packed_ptr_.load());
  }

  T* LoadPointer() const {
    auto stamped_ptr = Load();
    return stamped_ptr.raw_ptr_;
  }

  size_t GetStamp() const {
    auto stamped_ptr = Load();
    return stamped_ptr.stamp_;
  }

  void Store(StampedPtr stamped_ptr) {
    packed_ptr_.store(Pack(stamped_ptr));
  }

  // CompareAndSet automatically increments pointer's stamp on success:
  // (expected.raw_ptr_, expected.stamp_) -> (desired, expected.stamp_ + 1)

  bool CompareAndSet(const StampedPtr& expected, T* desired) {
    auto packed_expected = Pack(expected);
    return packed_ptr_.compare_exchange_strong(
        packed_expected, Pack(ProposeUpdate(expected, desired)));
  }

  bool CompareAndSet(StampedPtr& expected, T* desired) {
    auto packed_expected = Pack(expected);
    bool succeeded = packed_ptr_.compare_exchange_strong(
        packed_expected, Pack(ProposeUpdate(expected, desired)));
    if (!succeeded) {
      expected = Unpack(packed_expected);
    }
    return succeeded;
  }

 private:
  static StampedPtr ProposeUpdate(StampedPtr prev_stamped_ptr, T* new_raw_ptr) {
    return {new_raw_ptr, prev_stamped_ptr.stamp_ + 1};
  }

  static uintptr_t ResetHigh16Bits(uintptr_t ptr) {
    return (ptr << 16) >> 16;
  }

  static uintptr_t Pack(StampedPtr stamped_ptr) {
    return ResetHigh16Bits((uintptr_t)stamped_ptr.raw_ptr_) |
           (stamped_ptr.stamp_ << 48);
  }

  // For x86-64 pointers have a canonical form in which the upper 16 bits
  // are equal to bit 47 (zero-based)

  static uintptr_t ToCanonicalForm(uintptr_t ptr) {
    if (GetBit(ptr, 47)) {
      return ptr | ((uintptr_t)0xFFFF << 48);
    } else {
      return ResetHigh16Bits(ptr);
    }
  }

  static size_t GetStamp(uintptr_t packed_ptr) {
    return packed_ptr >> 48;
  }

  static T* GetRawPtr(uintptr_t packed_ptr) {
    return (T*)ToCanonicalForm(packed_ptr);
  }

  static StampedPtr Unpack(uintptr_t packed_ptr) {
    return {GetRawPtr(packed_ptr), GetStamp(packed_ptr)};
  }

 private:
  twist::atomic<uintptr_t> packed_ptr_;
};

}  // namespace lockfree
}  // namespace twist
