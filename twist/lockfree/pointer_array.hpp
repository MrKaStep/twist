#pragma once

#include <twist/stdlike/atomic.hpp>

#include <cassert>
#include <cstddef>
#include <cmath>

namespace twist {
namespace lockfree {

// Wait-free fixed-size array of atomic pointers
// Uses two-level page-table-like algorithm for lazy block allocation

template <typename T>
class PointerArray {
  using TAtomicPtr = twist::atomic<T*>;
  using TPtrBlock = TAtomicPtr*;
  using BlockPtr = twist::atomic<TPtrBlock>;

 public:
  PointerArray(const size_t size)
      : size_(size), block_size_(GetBlockSize(size)) {
    blocks_ = new BlockPtr[block_size_]();
  }

  ~PointerArray() {
    for (size_t i = 0; i < block_size_; ++i) {
      if (blocks_[i]) {
        delete[] blocks_[i].load();
      }
    }
    delete[] blocks_;
  }

  bool TrySet(const size_t index, T* ptr) {
    TAtomicPtr& slot = Access(index);
    T* null_ptr{nullptr};
    return slot.compare_exchange_strong(null_ptr, ptr);
  }

  T* Get(const size_t index) {
    TAtomicPtr& slot = Access(index);
    return slot.load();
  }

  T* operator[](size_t index) const {
    return Get();
  }

  TAtomicPtr& operator[](size_t index) {
    return Access(index);
  }

 private:
  static size_t GetBlockSize(const size_t size) {
    return static_cast<size_t>(sqrt(size) + 1);
  }

  TPtrBlock AllocateLeafBlock() {
    return new TAtomicPtr[block_size_]();  // fill with zeroes
  }

  TAtomicPtr& Access(const size_t index) {
    assert(index < size_);

    const size_t block_index = index / block_size_;
    const size_t index_in_block = index % block_size_;

    if (!blocks_[block_index]) {
      // Allocate block for provided index on first access
      TPtrBlock new_leaf_block = AllocateLeafBlock();

      TPtrBlock block_not_set{nullptr};
      auto& ptr_to_leaf_block = blocks_[block_index];
      if (!ptr_to_leaf_block.compare_exchange_strong(block_not_set,
                                                     new_leaf_block)) {
        delete[] new_leaf_block;
      }
    }

    return blocks_[block_index][index_in_block];
  }

 private:
  size_t size_;
  size_t block_size_;
  BlockPtr* blocks_{nullptr};
};

}  // namespace lockfree
}  // namespace twist
