#pragma once

#include <cstddef>

namespace twist {
namespace fiber {
namespace fls {

using Key = size_t;

size_t AcquireKey();
void ReleaseKey(Key key);

void* AccessSlot(Key key);

}  // namespace fls
}  // namespace fiber
}  // namespace twist
