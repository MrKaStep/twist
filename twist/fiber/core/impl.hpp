#pragma once

#include <twist/fiber/core/api.hpp>
#include <twist/fiber/core/context.hpp>
#include <twist/fiber/core/stack.hpp>

#include <twist/thread/stack.hpp>

#include <twist/support/intrusive_list.hpp>
#include <twist/support/time.hpp>

namespace twist {
namespace fiber {

//////////////////////////////////////////////////////////////////////

enum class FiberState { Starting, Runnable, Suspended, Terminated };

class Fiber : public IntrusiveListNode<Fiber> {
 public:
  size_t Id() const {
    return id_;
  }

  const FiberStack& Stack() const {
    return stack_;
  }

  ExecutionContext& Context() {
    return context_;
  }

  FiberState State() const {
    return state_;
  }

  void SetState(FiberState target) {
    state_ = target;
  }

  std::uintptr_t* LocalStorage() {
    return stack_.LocalStorage();
  }

  FiberRoutine UserRoutine() {
    return routine_;
  }

  static Fiber* Create(FiberRoutine routine);
  static void Release(Fiber* fiber);
  static void SetupTrampoline(Fiber* fiber);

 private:
  FiberStack stack_;
  ExecutionContext context_;
  FiberState state_;
  FiberRoutine routine_;
  FiberId id_;
};

using FiberQueue = IntrusiveList<Fiber>;

//////////////////////////////////////////////////////////////////////

Fiber* GetCurrentFiber();

//////////////////////////////////////////////////////////////////////

class Scheduler {
 public:
  Scheduler();

  // 'fuel' limits number of scheduling iterations (-1 means no limit)
  void Run(FiberRoutine main, size_t fuel = -1);

  void Spawn(FiberRoutine routine);
  void Yield();
  void SleepFor(Duration duration);
  void Suspend();
  void Resume(Fiber* that);
  void Terminate();

  // ignore upcoming Yield calls
  void PreemptDisable(bool disable = true) {
    preempt_disabled_ = disable;
  }

  // statistics
  size_t SwitchCount() const {
    return switch_count_;
  }

 private:
  void SpawnMainFiber(FiberRoutine main);

  void RunLoop(size_t fuel);
  void CheckDeadlock();

  Fiber* PickReadyFiber();
  void SwitchTo(Fiber* fiber);
  void Reschedule(Fiber* fiber);
  void Schedule(Fiber* fiber);

  Fiber* CreateFiber(FiberRoutine routine);
  void Destroy(Fiber* fiber);

 private:
  thread::Stack stack_;
  ExecutionContext context_;
  FiberQueue ready_;
  size_t alive_count_{0};

  bool preempt_disabled_{false};

  // statistics
  size_t switch_count_{0};
};

//////////////////////////////////////////////////////////////////////

Scheduler* GetCurrentScheduler();

//////////////////////////////////////////////////////////////////////

}  // namespace fiber
}  // namespace twist
