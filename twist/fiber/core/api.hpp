#pragma once

#include <twist/support/time.hpp>

#include <functional>

namespace twist {
namespace fiber {

//////////////////////////////////////////////////////////////////////

using FiberRoutine = std::function<void()>;

using FiberId = size_t;

//////////////////////////////////////////////////////////////////////

struct DeadlockDetected : public std::runtime_error {
  DeadlockDetected()
      : std::runtime_error("Deadlock detected in fiber scheduler") {
  }
};

// Run 'main' routine in fiber scheduler in current thread
void RunScheduler(FiberRoutine main, size_t fuel = -1);

//////////////////////////////////////////////////////////////////////

// This fiber functions

void Spawn(FiberRoutine routine);
void Yield();
void SleepFor(Duration duration);
FiberId GetFiberId();

}  // namespace fiber
}  // namespace twist
