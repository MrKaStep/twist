#include <twist/fiber/core/context.hpp>

#include <twist/fiber/core/asan.hpp>
#include <twist/fiber/core/stack.hpp>

#include <cstdint>

namespace twist {
namespace fiber {

// View for stack-saved registers
struct StackSavedContext {
  // Ordered from top to bottom of the stack

  void* rbp;
  void* rbx;

  void* r[4];

  static size_t Size() {
    return sizeof(StackSavedContext);
  }
};

void ExecutionContext::Setup(MemSpan stack, Trampoline trampoline) {
  stack_ = stack;

  // https://eli.thegreenplace.net/2011/02/04/where-the-top-of-the-stack-is-on-x86/

  StackBuilder builder(stack.Back());

  // Ensure trampoline will get 16-byte aligned frame pointer (rbp)
  // 'Next' here means first 'pushq %rbp' in trampoline prologue
  builder.AlignNextPush(16);

  // Prepare return address for DoSwitchContext
  builder.Push((uintptr_t)trampoline);

  // Reserve space for registers on stack
  builder.Allocate(StackSavedContext::Size());

  // Set current stack top
  rsp_ = builder.Top();
}

void ExecutionContext::SwitchTo(ExecutionContext& target) {
  AsanBeforeSwitchTo(target.stack_);
  DoSwitchContext(this, &target);
  AsanAfterSwitch();
}

}  // namespace fiber
}  // namespace twist
