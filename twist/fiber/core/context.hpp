#pragma once

#include <twist/memory/memspan.hpp>

#include <cstdlib>
#include <cstdint>

namespace twist {
namespace fiber {

struct ExecutionContext;

// Switch between ExecutionContext-s
extern "C" void DoSwitchContext(ExecutionContext* from, ExecutionContext* to);

// TODO(Lipovsky): closure instead of void(void) function
typedef void (*Trampoline)();

struct ExecutionContext {
  // Execution context stored on top of fiber/thread stack
  void* rsp_;

  // For AddressSanitizer annotations
  MemSpan stack_;

  void Setup(MemSpan stack) {
    stack_ = stack;
  }

  // Prepare execution context for running trampoline function
  void Setup(MemSpan stack, Trampoline trampoline);

  // Save current execution context to 'this' and jump to 'target' context
  // 'target' context created directly by Setup
  // or by another target.SwitchTo(smth) call
  void SwitchTo(ExecutionContext& target);
};

}  // namespace fiber
}  // namespace twist
