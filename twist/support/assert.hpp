#pragma once

#include <twist/support/compiler.hpp>
#include <twist/support/panic.hpp>

#define VERIFY(cond, error)                                   \
  do {                                                        \
    if (UNLIKELY(!(cond))) {                                  \
      PANIC("Assertion '" << #cond << "' failed: " << error); \
    }                                                         \
  } while (false);
