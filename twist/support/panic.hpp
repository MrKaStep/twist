#pragma once

#include <twist/support/source_location.hpp>
#include <twist/support/string_builder.hpp>

namespace detail {
void Panic(const std::string& error);
}  // namespace detail

// Print source location and error message to stderr, then abort
// Usage: PANIC("Internal error: " << e.what());

#define PANIC(error)                                           \
  do {                                                         \
    detail::Panic(StringBuilder() << HERE() << ": " << error); \
  } while (false)
