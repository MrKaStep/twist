#pragma once

#include <sanitizer/lsan_interface.h>

#if defined(LEAK_SANITIZER)

// Mark intentional memory leak
// The heap object referenced by pointer 'ptr' will be annotated as a leak.

inline void MarkLeakingObjectPtr(void* ptr) {
  __lsan_ignore_object(ptr);
}

#else

inline void MarkLeakingObjectPtr(void*) {
}

#endif
