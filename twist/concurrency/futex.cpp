#include <twist/concurrency/futex.hpp>

#include <atomic>
#include <thread>

#if LINUX

#include <twist/thread/native_futex.hpp>

#include <cassert>
#include <climits>

#else

#include <mutex>
#include <condition_variable>

#endif

namespace twist {

#if LINUX

////////////////////////////////////////////////////////////////////////////////

class Futex::Impl {
 public:
  Impl(std::atomic<uint32_t>& atomic) : atomic_(atomic) {
    static_assert(sizeof(atomic_) == 4, "Expected 4-byte atomic");
  }

  void Wait(uint32_t expected) {
    assert(expected < INT_MAX);

    int retcode = twist::thread::FutexWait(Addr(), expected);

    if (IsUnexpectedValue(retcode)) {
      return;  // immediately
    }

    assert(retcode == 0);
  }

  void WakeOne() {
    int retcode = twist::thread::FutexWake(Addr(), 1);
    assert(retcode >= 0);
  }

  void Wake(int count) {
    int retcode = twist::thread::FutexWake(Addr(), count);
    assert(retcode >= 0);
  }

  void WakeAll() {
    Wake(INT_MAX);
  }

 private:
  static bool IsUnexpectedValue(int retcode) {
    return retcode < 0 && errno == EAGAIN;
  }

  unsigned int* Addr() {
    return reinterpret_cast<unsigned int*>(&atomic_);
  }

 private:
  std::atomic<uint32_t>& atomic_;
};

#else

////////////////////////////////////////////////////////////////////////////////

class Futex::Impl {
 public:
  Impl(std::atomic<uint32_t>& atomic) : atomic_(atomic) {
  }

  void Wait(uint32_t expected) {
    std::unique_lock<std::mutex> lock(mutex_);
    if (atomic_.load() == expected) {
      queued_.wait(lock);
    }
  }

  void WakeOne() {
    std::lock_guard<std::mutex> lock(mutex_);
    queued_.notify_one();
  }

  void WakeAll() {
    std::lock_guard<std::mutex> lock(mutex_);
    queued_.notify_all();
  }

  void Wake(int count) {
    if (count > 1) {
      WakeAll();
    } else if (count == 1) {
      WakeOne();
    }
  }

 private:
  std::atomic<uint32_t>& atomic_;
  std::mutex mutex_;
  std::condition_variable queued_;
};

#endif

////////////////////////////////////////////////////////////////////////////////

// PImpl idiom

Futex::Futex(std::atomic<uint32_t>& atomic) : impl_(new Futex::Impl(atomic)) {
}

Futex::~Futex() {
}

static void SwitchIfFlaky() {
#if defined(FLAKY)
  static std::atomic<size_t> call_count{0};

  size_t this_call_index = call_count.fetch_add(1, std::memory_order_relaxed);
  if (this_call_index % 7 == 0) {
    std::this_thread::yield();
  }
#endif
}

void Futex::Wait(uint32_t expected) {
  SwitchIfFlaky();
  impl_->Wait(expected);
  SwitchIfFlaky();
}

void Futex::WakeOne() {
  SwitchIfFlaky();
  impl_->WakeOne();
  SwitchIfFlaky();
}

void Futex::Wake(int count) {
  SwitchIfFlaky();
  impl_->Wake(count);
  SwitchIfFlaky();
}

void Futex::WakeAll() {
  SwitchIfFlaky();
  impl_->WakeAll();
  SwitchIfFlaky();
}

}  // namespace twist
