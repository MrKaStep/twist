#pragma once

#include <atomic>
#include <memory>

namespace twist {

class Futex {
 public:
  Futex(std::atomic<uint32_t>& atomic);
  ~Futex();

  // Put thread to sleep if value of atomic is equal to expected
  void Wait(uint32_t expected);

  void WakeOne();
  void Wake(int count);
  void WakeAll();

 private:
  class Impl;
  std::unique_ptr<Impl> impl_;
};

}  // namespace twist
