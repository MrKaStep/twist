#pragma once

#include <twist/thread/spinlock.hpp>
#include <twist/threading/stdlike.hpp>

#include <thread>

namespace twist {

class SpinWait {
  static const size_t kIterationsBeforeYield = 100;

 public:
  void operator()() noexcept {
    SpinOnce();
  }

  void SpinOnce() noexcept;

  void Reset() {
    iteration_count_ = 0;
  }

 private:
  // Implementation for threads backend
  void SpinOnceMultiCore() {
    iteration_count_++;
    if (iteration_count_ < kIterationsBeforeYield) {
      twist::thread::SpinLockPause();
    } else {
      std::this_thread::yield();
    }
  }

  // Implementation for fiber backend
  void SpinOnceSingleCore() {
    // Yield immediately
    twist::th::this_thread::yield();
  }

 private:
  size_t iteration_count_{0};
};

}  // namespace twist
