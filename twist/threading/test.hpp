#pragma once

#include <twist/test_framework/test_framework.hpp>
#include <twist/threading/context.hpp>
#include <twist/flaky/adversary.hpp>
#include <twist/support/preprocessor.hpp>

#include <chrono>
#include <functional>
#include <vector>

// _T stands for Threading

////////////////////////////////////////////////////////////////////////////////

#define SIMPLE_T_TEST(name)                                      \
  void SimpleThreadingTest##name();                              \
  SIMPLE_TEST(name) {                                            \
    twist::th::RunInThreadingContext(SimpleThreadingTest##name); \
  }                                                              \
  void SimpleThreadingTest##name()

////////////////////////////////////////////////////////////////////////////////

using Duration = std::chrono::nanoseconds;

struct TTestParameters {
 public:
  TTestParameters(std::initializer_list<size_t> values) : values_(values) {
  }

  TTestParameters& TimeLimit(std::chrono::nanoseconds value) {
    time_limit_ = value;
    return *this;
  }

  size_t Get(size_t index) const {
    return values_.at(index);
  }

  friend std::ostream& operator<<(::std::ostream& out,
                                  const TTestParameters& parameters);

 public:
  std::vector<size_t> values_;
  Duration time_limit_{std::chrono::minutes(1)};
};

////////////////////////////////////////////////////////////////////////////////

namespace detail {

using TTestRoutine = std::function<void(TTestParameters)>;

class TTestCase : public ITest {
 public:
  TTestCase(std::string name, TTestRoutine routine, TTestParameters params)
      : name_(std::move(name)), routine_(std::move(routine)), params_(params) {
  }

  void Run() override {
    TestTimeLimitWatcher time_limit_watcher(params_.time_limit_);
    RunTestInThreadingContext();
  }

  std::string Name() const override {
    return name_;
  }

  std::string Describe() const override {
    return StringBuilder() << name_ << "(" << params_ << ")";
  }

 private:
  void RunTestInThreadingContext() {
    auto wrapper = [this]() {
      twist::flaky::GetAdversary()->TestStarted();
      try {
        routine_(params_);
      } catch (...) {
        FailTestByException();
      }
      twist::flaky::GetAdversary()->TestCompleted();
    };
    twist::th::RunInThreadingContext(wrapper);
  }

 private:
  std::string name_;
  TTestRoutine routine_;
  TTestParameters params_;
};

////////////////////////////////////////////////////////////////////////////////

class TTestCaseRegistrar {
  static constexpr Duration kDefaultTimeLimit = std::chrono::seconds(10);

 public:
  TTestCaseRegistrar(std::string name, TTestRoutine routine)
      : name_(std::move(name)),
        routine_(std::move(routine)),
        time_limit_(kDefaultTimeLimit) {
  }

  using Self = TTestCaseRegistrar;

  Self& TimeLimit(Duration time_limit) {
    time_limit_ = time_limit;
    return *this;
  }

  Self& Case(std::initializer_list<size_t> values) {
    TTestParameters params(std::move(values));
    params.time_limit_ = time_limit_;

    RegisterTest(std::make_shared<TTestCase>(name_, routine_, params));

    return *this;
  }

 private:
  std::string name_;
  TTestRoutine routine_;
  Duration time_limit_;
};

}  // namespace detail

////////////////////////////////////////////////////////////////////////////////

#define UNIQUE_REGISTRAR_NAME(test) CONCAT(CONCAT(test, __LINE__), __registrar)

#define T_TEST_CASES(test)                                        \
  static detail::TTestCaseRegistrar UNIQUE_REGISTRAR_NAME(test) = \
      detail::TTestCaseRegistrar(#test, test)
