#include <twist/threading/test.hpp>

////////////////////////////////////////////////////////////////////////////////

std::ostream& operator<<(::std::ostream& out,
                         const TTestParameters& parameters) {
  out << "{";
  for (size_t i = 0; i < parameters.values_.size(); ++i) {
    if (i > 0) {
      out << ", ";
    }
    out << parameters.values_[i];
  }
  out << "}";
  return out;
}

////////////////////////////////////////////////////////////////////////////////
