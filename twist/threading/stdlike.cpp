#include <twist/threading/stdlike.hpp>

#if defined(FIBER)

#pragma message("Threading backend: fibers")

#else

#pragma message("Threading backend: threads")

#endif
