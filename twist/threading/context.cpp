#include <twist/threading/context.hpp>

#if defined(FIBER)
#include <twist/fiber/core/api.hpp>
#endif

namespace twist {
namespace th {

void RunInThreadingContext(std::function<void()> routine) {
#if defined(FIBER)
  twist::fiber::RunScheduler(routine);
#else  // threads
  routine();
#endif
}

}  // namespace th
}  // namespace twist
