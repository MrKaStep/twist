#pragma once

#include <functional>

namespace twist {
namespace th {

void RunInThreadingContext(std::function<void()> routine);

}  // namespace th
}  // namespace twist
