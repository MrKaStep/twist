#include <twist/threading/spin_wait.hpp>

#if defined(FLAKY) || defined(FIBER) || PROC_COUNT == 1

#define SPIN_MULTI_CORE 0
#pragma message("SpinWait in single-core mode")

#else

#define SPIN_MULTI_CORE 1
#pragma message("SpinWait in multi-core mode")

#endif

namespace twist {

void SpinWait::SpinOnce() noexcept {
#if (SPIN_MULTI_CORE)
  SpinOnceMultiCore();
#else
  SpinOnceSingleCore();
#endif
}

}  // namespace twist
