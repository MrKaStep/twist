cmake_minimum_required(VERSION 3.5)

# --------------------------------------------------------------------

get_filename_component(LIB_INCLUDE_PATH ".." ABSOLUTE)
get_filename_component(LIB_PATH "." ABSOLUTE)

file(GLOB_RECURSE LIB_SOURCES ${LIB_PATH}/*.cpp ${LIB_PATH}/*.S)
file(GLOB_RECURSE LIB_HEADERS ${LIB_PATH}/*.hpp)

enable_language(ASM)

add_library(twist STATIC ${LIB_SOURCES} ${LIB_HEADERS})
target_include_directories(twist PUBLIC ${LIB_INCLUDE_PATH})
target_link_libraries(twist PUBLIC pthread)

# --------------------------------------------------------------------

set(TEST_FRAMEWORK_DEFINITIONS FORK_TESTS=1 NO_TEST_TIME_LIMIT=0)

target_compile_definitions(twist PUBLIC ${TEST_FRAMEWORK_DEFINITIONS})

if(FLAKY)
    target_compile_definitions(twist PUBLIC FLAKY=1)
endif()

if(FIBER)
    target_compile_definitions(twist PUBLIC FIBER=1)
endif()

