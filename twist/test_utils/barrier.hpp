#pragma once

#include <twist/threading/stdlike.hpp>

namespace twist {

class OnePassBarrier {
 public:
  explicit OnePassBarrier(const size_t num_threads)
      : thread_count_{num_threads} {
  }

  void PassThrough() {
    std::unique_lock<twist::th::mutex> lock{mutex_};
    --thread_count_;
    if (thread_count_ == 0) {
      all_threads_arrived_.notify_all();
    } else {
      all_threads_arrived_.wait(lock, [this]() { return thread_count_ == 0; });
    }
  }

 private:
  twist::th::mutex mutex_;
  twist::th::condition_variable all_threads_arrived_;
  size_t thread_count_;
};

}  // namespace twist
