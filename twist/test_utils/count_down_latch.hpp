#pragma once

#include <twist/threading/stdlike.hpp>
#include <twist/support/locking.hpp>

namespace twist {

// Allows one or more threads to wait until a set of operations being performed
// in other threads completes

class CountDownLatch {
 public:
  CountDownLatch(size_t count) : count_(count) {
  }

  // Decrements the count of the latch, releasing all waiting threads if the
  // count reaches zero

  void CountDown() {
    auto lock = twist::LockUnique(mutex_);
    --count_;
    if (count_ == 0) {
      released_.notify_all();
    }
  }

  // Wait until the latch has counted down to zero

  void Await() {
    auto lock = twist::LockUnique(mutex_);
    while (count_ > 0) {
      released_.wait(lock);
    }
  }

 private:
  size_t count_;
  twist::th::mutex mutex_;
  twist::th::condition_variable released_;
};

}  // namespace twist
