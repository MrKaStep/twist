#pragma once

#include <twist/test_framework/test_framework.hpp>
#include <twist/threading/stdlike.hpp>
#include <twist/test_utils/affinity.hpp>

#include <functional>
#include <vector>

namespace twist {

class ScopedExecutor {
  using Task = std::function<void()>;

 public:
  void Submit(Task task);

  template <typename... Args>
  void Submit(Args&&... args) {
    Submit(Task(std::bind(std::forward<Args>(args)...)));
  }

  ~ScopedExecutor() {
    Join();
  }

  void Join();

 private:
  static void Run(Task task);

 private:
  std::vector<twist::th::thread> threads_;
  bool joined_{false};
};

////////////////////////////////////////////////////////////////////////////////

template <typename... Args>
twist::th::thread RunThread(Args&&... args) {
  std::function<void()> task(std::bind(std::forward<Args>(args)...));

  auto wrapped_task = [task]() {
    SetTestThreadAffinity();
    try {
      task();
    } catch (...) {
      FailTestByException();
    }
  };

  return twist::th::thread(wrapped_task);
}

}  // namespace twist
