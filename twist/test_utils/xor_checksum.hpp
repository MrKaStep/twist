#pragma once

#include <twist/thread/thread_local.hpp>

#include <functional>

namespace twist {

class XORCheckSum {
 public:
  template <typename T>
  void Feed(const T& item) {
    *locals_ ^= std::hash<T>{}(item);
  }

  bool Validate() {
    size_t total = 0;
    for (auto& local : locals_) {
      total ^= local;
    }
    return total == 0;
  }

 private:
  twist::thread::ThreadLocal<size_t> locals_{0};
};

}  // namespace twist
