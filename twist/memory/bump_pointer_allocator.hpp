#pragma once

#include <atomic>
#include <cstddef>
#include <new>
#include <stdexcept>
#include <type_traits>
#include <utility>

namespace twist {

namespace detail {

constexpr size_t AlignUpTo8(size_t size) {
  return (size + 7) & -8;
}

template <typename Object>
void CallDestructor(void* ptr) {
  reinterpret_cast<Object*>(ptr)->~Object();
}

typedef void (*ObjectDeleterPtr)(void*);

}  // namespace detail

// Simple bump pointer allocator with bounded capacity

class BumpPointerAllocator {
 private:
  struct ObjectHeader {
    size_t block_size_;
    detail::ObjectDeleterPtr deleter_;

    ObjectHeader(size_t block_size, detail::ObjectDeleterPtr deleter)
        : block_size_(block_size), deleter_(deleter) {
    }
  };

 public:
  BumpPointerAllocator(size_t arena_size)
      : arena_size_(arena_size),
        arena_(AllocateArena(arena_size)),
        next_block_ptr_(arena_) {
    static_assert(sizeof(ObjectHeader) % 8 == 0,
                  "unexpected ObjectHeader size");
  }

  template <typename Object, typename... Args>
  Object* New(Args&&... args) {
    char* object_block_ptr = AllocateObjectBlock<Object>();
    Object* object_ptr = reinterpret_cast<Object*>(object_block_ptr);
    new (object_ptr) Object(std::forward<Args>(args)...);
    return object_ptr;
  }

  void Reset() {
    DestroyAllocatedObjects();
    next_block_ptr_ = arena_;
  }

  void FastReset() {
    // skip all destructors
    next_block_ptr_ = arena_;
  }

  ~BumpPointerAllocator() {
    Reset();
    DeallocateArena();
  }

 private:
  static char* AllocateArena(size_t size) {
    return new char[size];
  }

  void DeallocateArena() {
    delete[] arena_;
  }

  char* ArenaEnd() const {
    return arena_ + arena_size_;
  }

  char* AllocateBlock(size_t size) {
    char* block_ptr = next_block_ptr_.fetch_add(size);
    if (block_ptr + size >= ArenaEnd()) {
      throw std::runtime_error("Arena overflow");
    }
    return block_ptr;
  }

  template <typename Object>
  char* AllocateObjectBlock() {
    size_t object_block_size = detail::AlignUpTo8(sizeof(Object));
    size_t block_size =
        sizeof(ObjectHeader) + object_block_size;  // header + object
    char* block_ptr = AllocateBlock(block_size);
    WriteBlockHeader(block_ptr, block_size, &detail::CallDestructor<Object>);
    return block_ptr;
  }

  void WriteBlockHeader(char*& block_ptr, size_t block_size,
                        detail::ObjectDeleterPtr deleter) {
    auto* header_ptr = reinterpret_cast<ObjectHeader*>(block_ptr);
    new (header_ptr) ObjectHeader(block_size, deleter);
    block_ptr += sizeof(ObjectHeader);
  }

  void DestroyAllocatedObjects() {
    char* block_ptr = arena_;
    char* end_block_ptr = next_block_ptr_.load();

    while (block_ptr != end_block_ptr) {
      auto* header = reinterpret_cast<ObjectHeader*>(block_ptr);
      auto* object_block_ptr = block_ptr + sizeof(ObjectHeader);
      header->deleter_(object_block_ptr);
      block_ptr += header->block_size_;
    }
  }

 private:
  size_t arena_size_;
  char* arena_;
  std::atomic<char*> next_block_ptr_;
};

}  // namespace twist
