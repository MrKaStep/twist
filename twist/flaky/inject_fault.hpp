#pragma once

// Voluntarily inject fault (context switch or suspension) into current thread

#if defined(FLAKY)

#include <twist/flaky/adversary.hpp>

#define INJECT_FAULT() twist::flaky::GetAdversary()->Fault();

#else

#define INJECT_FAULT()

#endif
