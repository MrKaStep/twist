#pragma once

#include <twist/flaky/adversary.hpp>
#include <twist/threading/stdlike.hpp>

namespace twist {
namespace flaky {

class FlakyMutex {
 public:
  // std::mutex-like / Lockable

  void lock() {  // NOLINT
    InjectFault();
    impl_.lock();
    InjectFault();
  }

  bool try_lock() {  // NOLINT
    InjectFault();
    bool acquired = impl_.try_lock();
    InjectFault();
    return acquired;
  }

  void unlock() {  // NOLINT
    InjectFault();
    impl_.unlock();
    InjectFault();
  }

 private:
  twist::th::mutex impl_;
};

}  // namespace flaky
}  // namespace twist
