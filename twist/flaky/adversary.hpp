#pragma once

#include <memory>

namespace twist {
namespace flaky {

/////////////////////////////////////////////////////////////////////

class IAdversary {
 public:
  virtual ~IAdversary() = default;

  // Test

  virtual void TestStarted() = 0;
  virtual void TestCompleted() = 0;

  // This thread

  virtual void Enter() = 0;

  // Inject fault (yield, sleep, park)
  virtual void Fault() = 0;

  // For lock-free and wait-free algorithms
  virtual void ReportProgress() = 0;

  virtual void Exit() = 0;
};

using IAdversaryPtr = std::shared_ptr<IAdversary>;

/////////////////////////////////////////////////////////////////////

// Not thread safe, should be externally synchronized

IAdversary* GetAdversary();
void SetAdversary(IAdversaryPtr adversary);

/////////////////////////////////////////////////////////////////////

void InjectFault();

}  // namespace flaky
}  // namespace twist
