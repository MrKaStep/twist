#pragma once

#include <twist/flaky/mutex.hpp>

#include <memory>
#include <mutex>

namespace twist {
namespace flaky {

class FlakyConditionVariable {
 public:
  using Lock = std::unique_lock<FlakyMutex>;

  FlakyConditionVariable();
  ~FlakyConditionVariable();

  void wait(Lock& lock);  // NOLINT

  template <class Predicate>
  void wait(Lock& lock, Predicate predicate) {  // NOLINT
    while (!predicate()) {
      wait(lock);
    }
  }

  void notify_one();  // NOLINT
  void notify_all();  // NOLINT

 private:
  class Impl;
  std::unique_ptr<Impl> pimpl_;
};

}  // namespace flaky
}  // namespace twist
