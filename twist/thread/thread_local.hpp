#pragma once

#include <twist/thread/tls.hpp>

#include <twist/support/noncopyable.hpp>
#include <twist/stdlike/atomic.hpp>

#include <atomic>
#include <cassert>
#include <cstring>
#include <functional>

namespace twist {
namespace thread {

/* Provides thread-local storage of data
 * Usage: ThreadLocal<T> var{default};
 * 1) *var = value / auto value = *var - access value for this instance for the
 * current thread
 * 2) for (auto& value : var) { ... } - iterate over values
 * currently stored by all of the threads that have accessed this instance
 * 3) var.GetThreadCount() - get number of threads that have accessed this
 * instance
 */

template <typename T>
class ThreadLocal : NonCopyable {
  struct ValueNode {
    T value_;
    ValueNode* next_{nullptr};

    ValueNode(T&& value) : value_(std::move(value)) {
    }
  };

  using ValueInitializer = std::function<T(void)>;

 public:
  ThreadLocal() : ThreadLocal([]() { return T{}; }) {
  }

  explicit ThreadLocal(T default_value)
      : ThreadLocal([default_value]() { return T{default_value}; }) {
  }

  explicit ThreadLocal(ValueInitializer initializer)
      : value_initializer_(initializer) {
    tls_key_ = tls::AcquireKey();
  }

  ~ThreadLocal() {
    ClearValuesList();
  }

  // access thread local value

  T& Get() {
    auto* thread_value_node = AccessThreadValueNode();
    return thread_value_node->value_;
  }

  T& operator*() {
    return Get();
  }

  T* operator->() {
    auto* thread_value_node = AccessThreadValueNode();
    return &(thread_value_node->value_);
  }

  // estimate number of threads

  size_t GetThreadCount() const {
    return thread_count_.load();
  }

  // iterate over values of all threads

  class Iterator {
   public:
    Iterator(ValueNode* head) : current_(head) {
    }

    bool IsValid() const {
      return current_ != nullptr;
    }

    T& operator*() {
      return current_->value_;
    }

    T* operator->() {
      return &current_->value_;
    }

    bool operator==(const Iterator& that) const {
      return current_ == that.current_;
    }

    bool operator!=(const Iterator& that) const {
      return !(*this == that);
    }

    void operator++() {
      assert(IsValid());
      current_ = current_->next_;
    }

   private:
    ValueNode* current_;
  };

  // support range-based for loops

  Iterator begin() {  // NOLINT
    return Iterator{thread_values_head_.load()};
  }

  Iterator end() {  // NOLINT
    return Iterator{nullptr};
  }

  // TODO: const begin/end

 private:
  ValueNode* AccessThreadValueNode() {
    // slot in TLS stores pointer to value node
    void** slot = (void**)tls::AccessSlot(tls_key_);

    if (*slot == nullptr) {
      // slow path
      *slot = (void*)AllocateThreadValueNodeForThisThread();
    }

    return (ValueNode*)*slot;
  }

  void AddNodeToValuesList(ValueNode* thread_value_node) {
    thread_value_node->next_ = thread_values_head_.load();
    while (!thread_values_head_.compare_exchange_weak(thread_value_node->next_,
                                                      thread_value_node)) {
      // retry
    }
  }

  ValueNode* AllocateThreadValueNodeForThisThread() {
    ValueNode* thread_value_node = new ValueNode(value_initializer_());
    AddNodeToValuesList(thread_value_node);
    thread_count_.fetch_add(1);
    return thread_value_node;
  }

  void ClearValuesList() {
    ValueNode* current = thread_values_head_.load();
    while (current != nullptr) {
      ValueNode* next = current->next_;
      delete current;
      current = next;
    }
  }

 private:
  ValueInitializer value_initializer_;
  std::atomic<ValueNode*> thread_values_head_{nullptr};
  std::atomic<size_t> thread_count_{0};
  size_t tls_key_;
};

}  // namespace thread
}  // namespace twist
