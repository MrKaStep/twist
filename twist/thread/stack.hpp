#pragma once

#include <cstddef>

#include <twist/memory/memspan.hpp>

namespace twist {
namespace thread {

struct Stack {
  char* start_;
  size_t size_;

  char* Bottom() const {
    return start_ + size_ - 1;
  }

  size_t Size() const {
    return size_;
  }

  MemSpan AsMemSpan() const {
    return MemSpan(start_, size_);
  }

  static Stack ThisThread();

 private:
  Stack() = default;
};

}  // namespace thread
}  // namespace twist
