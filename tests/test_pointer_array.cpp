#include <twist/lockfree/pointer_array.hpp>

#include <twist/test_framework/test_framework.hpp>

#include <chrono>
#include <thread>

TEST_SUITE(PointerArray) {
  SIMPLE_TEST(SetAndGet) {
    twist::lockfree::PointerArray<int> int_ptrs{100};

    ASSERT_EQ(int_ptrs.Get(83), nullptr);

    int* int_ptr = new int{42};
    ASSERT_TRUE(int_ptrs.TrySet(83, int_ptr));
    ASSERT_TRUE(int_ptrs.Get(83) == int_ptr);
    //delete int_ptr;
  }

  SIMPLE_TEST(Bounds) {
    twist::lockfree::PointerArray<int> int_ptrs{100};

    ASSERT_EQ(int_ptrs.Get(0), nullptr);
    ASSERT_EQ(int_ptrs.Get(42), nullptr);
    ASSERT_EQ(int_ptrs.Get(99), nullptr);
  }

  SIMPLE_TEST(Brackets) {
    int value = 42;
    twist::lockfree::PointerArray<int> array{17};
    array[11] = &value;
    ASSERT_EQ(array[11], &value);
  }
}
