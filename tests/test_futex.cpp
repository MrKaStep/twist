#include <twist/concurrency/futex.hpp>

#include <twist/test_framework/test_framework.hpp>

#include <chrono>
#include <thread>

TEST_SUITE(Futex) {
  SIMPLE_TEST(NoWait) {
    std::atomic<uint32_t> word{0};
    twist::Futex futex(word);

    // Returns immediately
    futex.Wait(1);
  }

  SIMPLE_TEST(Wait) {
    std::atomic<uint32_t> word{1};
    twist::Futex futex(word);

    auto wake = [&futex]() {
      std::this_thread::sleep_for(std::chrono::milliseconds(500));
      futex.WakeOne();
    };

    std::thread background(wake);

    futex.Wait(1);

    background.join();
  }
}
