#include <twist/flaky/condvar.hpp>

#include <twist/flaky/mutex.hpp>

#include <twist/support/compiler.hpp>
#include <twist/support/sleep.hpp>
#include <twist/test_framework/test_framework.hpp>

#include <thread>

using namespace twist::flaky;

#if !defined(FIBER)

// run this tests only with threads

TEST_SUITE(FlakyConditionVariable) {
  SIMPLE_TEST(NotifyOne) {
    FlakyMutex mutex;
    bool ready = false;
    FlakyConditionVariable condvar;

    auto notify_routine = [&]() {
      twist::SleepMillis(500);

      std::lock_guard<FlakyMutex> lock(mutex);
      ready = true;
      condvar.notify_one();
    };

    {
      std::unique_lock<FlakyMutex> lock(mutex);

      std::thread thread(notify_routine);

      condvar.wait(lock, [&]() { return ready; });

      thread.join();
    }
  }

  SIMPLE_TEST(NotifyAll) {
    FlakyMutex mutex;
    size_t count = 5;
    FlakyConditionVariable all_arrived;

    auto pass_routine = [&]() {
      std::unique_lock<FlakyMutex> lock(mutex);
      --count;
      if (count == 0) {
        all_arrived.notify_all();
      } else {
        while (count > 0) {
          all_arrived.wait(lock);
        }
      }
    };

    std::vector<std::thread> threads;
    for (size_t i = 0; i < 5; ++i) {
      threads.emplace_back(pass_routine);
    }
    for (auto& t : threads) {
      t.join();
    }
  }
}

#endif
