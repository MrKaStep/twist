#include <twist/lockfree/x86_64_atomic_stamped_pointer.hpp>

#include <twist/test_framework/test_framework.hpp>

template <typename T>
using AtomicStampedPtr = twist::lockfree::X86AtomicStampedPointer<T>;

TEST_SUITE(X86AtomicStampedPointer) {
  SIMPLE_TEST(DefaultPointer) {
    AtomicStampedPtr<int> stamped_ptr;
    ASSERT_EQ(stamped_ptr.LoadPointer(), nullptr);
    ASSERT_EQ(stamped_ptr.GetStamp(), 0);
  }

  SIMPLE_TEST(Ctor) {
    int* ptr = new int{17};
    AtomicStampedPtr<int> stamped_ptr(ptr);
    ASSERT_EQ(stamped_ptr.LoadPointer(), ptr);
    delete ptr;
  }

  struct Node {
    int data_;
    Node* next_;

    explicit Node(int data) : data_(data) {
    }
  };

  SIMPLE_TEST(StampedPtrApi) {
    Node* raw_ptr = new Node{42};
    AtomicStampedPtr<Node> atomic_stamped_ptr{raw_ptr};
    auto stamped_ptr = atomic_stamped_ptr.Load();
    ASSERT_EQ(stamped_ptr->data_, 42);
    ASSERT_TRUE(stamped_ptr);
  }

  SIMPLE_TEST(StoreLoadReset) {
    int* ptr = new int{42};
    AtomicStampedPtr<int> stamped_ptr;

    stamped_ptr.Store({ptr, 17});
    auto unpacked_stamped_ptr = stamped_ptr.Load();
    ASSERT_EQ(unpacked_stamped_ptr.raw_ptr_, ptr);
    ASSERT_EQ(unpacked_stamped_ptr.stamp_, 17);
    ASSERT_EQ(*unpacked_stamped_ptr, 42);

    delete ptr;
    ptr = new int{43};

    stamped_ptr.Store({ptr, 77});
    ASSERT_EQ(stamped_ptr.LoadPointer(), ptr);
    ASSERT_EQ(stamped_ptr.GetStamp(), 77);

    delete ptr;
  }

  SIMPLE_TEST(CompareAndSet) {
    AtomicStampedPtr<int> stamped_ptr;

    int* ptr = new int{42};
    ASSERT_TRUE(stamped_ptr.CompareAndSet({nullptr, 0}, ptr));
    ASSERT_EQ(stamped_ptr.GetStamp(), 1);

    ASSERT_FALSE(stamped_ptr.CompareAndSet({ptr, 0}, nullptr));

    ASSERT_TRUE(stamped_ptr.CompareAndSet({ptr, 1}, nullptr));
    ASSERT_EQ(stamped_ptr.GetStamp(), 2);

    delete ptr;
  }

  SIMPLE_TEST(StampIncrement) {
    AtomicStampedPtr<int> stamped_ptr;
    static const size_t kIterations = 1024;
    for (size_t i = 0; i < kIterations; ++i) {
      ASSERT_TRUE(stamped_ptr.CompareAndSet({nullptr, i}, nullptr));
    }
    ASSERT_EQ(stamped_ptr.GetStamp(), kIterations);
  }

  SIMPLE_TEST(StampOverflow) {
    AtomicStampedPtr<int> stamped_ptr;

    static const size_t kUInt16Max = (1 << 16) - 1;
    for (size_t i = 0; i < kUInt16Max; ++i) {
      ASSERT_TRUE(stamped_ptr.CompareAndSet({nullptr, i}, nullptr));
    }
    ASSERT_EQ(stamped_ptr.GetStamp(), kUInt16Max);
    ASSERT_TRUE(stamped_ptr.CompareAndSet({nullptr, kUInt16Max}, nullptr)); // overflow
    ASSERT_EQ(stamped_ptr.GetStamp(), 0);
    for (size_t i = 0; i < 1024; ++i) {
      ASSERT_TRUE(stamped_ptr.CompareAndSet({nullptr, i}, nullptr));
    }
  }

  SIMPLE_TEST(CASExpectedByRef) {
    AtomicStampedPtr<int> stamped_ptr;

    int* raw_ptr = new int{42};
    auto expected = stamped_ptr.Load();

    ASSERT_TRUE(stamped_ptr.CompareAndSet(expected, raw_ptr));
    ASSERT_EQ(expected.raw_ptr_, nullptr);
    ASSERT_EQ(expected.stamp_, 0);

    ASSERT_FALSE(stamped_ptr.CompareAndSet(expected, raw_ptr));
    ASSERT_EQ(expected.raw_ptr_, raw_ptr);
    ASSERT_EQ(expected.stamp_, 1);

    delete raw_ptr;
  }
}
