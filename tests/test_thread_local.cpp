#include <twist/thread/thread_local.hpp>
#include <twist/concurrency/barrier.hpp>

#include <twist/test_framework/test_framework.hpp>

#include <string>
#include <thread>
#include <vector>

TEST_SUITE(ThreadLocal) {
  SIMPLE_TEST(InitializeAndGet) {
    twist::thread::ThreadLocal<int> tl_int{42};
    ASSERT_EQ(*tl_int, 42); // allocate node
    ASSERT_EQ(tl_int.Get(), 42);

    twist::thread::ThreadLocal<int> tl_other_int{17};
    ASSERT_EQ(*tl_other_int, 17);
    ASSERT_EQ(*tl_int, 42); // recheck
  }

  SIMPLE_TEST(AccessFromDifferentThreads) {
    twist::thread::ThreadLocal<std::string> tl_string("first");

    ASSERT_EQ(*tl_string, "first");

    std::thread other_thread([&tl_string]() {
      *tl_string = "second";
      ASSERT_EQ(*tl_string, "second");
    });
    other_thread.join();

    ASSERT_EQ(*tl_string, "first"); // still same
  }

  SIMPLE_TEST(ConstructTLInDifferentThreads) {
    static const size_t kThreads = 2;
    twist::thread::ThreadLocal<int> tl_int;
    twist::thread::ThreadLocal<int>* tl[kThreads];

    tl_int.Get() = 42;
    auto thread_routine = [&tl_int, &tl](int thread_index) {
      tl_int.Get() = 42;
      tl[thread_index] = new twist::thread::ThreadLocal<int>;
      tl[thread_index]->Get() = thread_index;
    };

    std::vector<std::thread> threads;
    for (size_t i = 0; i < kThreads; ++i) {
      threads.emplace_back(thread_routine, i);
    }
    for (auto& thread : threads) {
      thread.join();
    }

    for (auto& value : tl_int) {
      ASSERT_EQ(value, 42);
    }

    for (size_t i = 0; i < kThreads; ++i) {
      delete tl[i];
    }
  }

  SIMPLE_TEST(IterateOverThreadValues) {
    static const size_t kThreads = 10;

    twist::thread::ThreadLocal<int> tl_int;

    ASSERT_EQ(tl_int.GetThreadCount(), 0); // not accessed yet

    twist::OnePassBarrier barrier{kThreads};

    auto thread_routine = [&tl_int, &barrier](int thread_index) {
      *tl_int = thread_index;

      barrier.PassThrough();

      ASSERT_EQ(tl_int.GetThreadCount(), kThreads);

      size_t total_value = 0;
      size_t thread_count = 0;
      for (auto& value : tl_int) {
        ++thread_count;
        total_value += value;
      }

      ASSERT_EQ(thread_count, kThreads);
      ASSERT_EQ(total_value, kThreads * (kThreads - 1) / 2);
    };

    std::vector<std::thread> threads;
    for (size_t i = 0; i < kThreads; ++i) {
      threads.emplace_back(thread_routine, i);
    }
    for (auto& thread : threads) {
      thread.join();
    }
  }
}
